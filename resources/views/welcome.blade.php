@extends('layouts.app') @section('content')
    <div class="w-full lg:w-1/2 mx-auto  mt-2 mb-2 p-2 md:p-0 shadow-lg">
        <img class=" rounded-lg"
             alt="Logo Aziendale"
             src="http://drogheria.polione.xyz/wp-content/themes/base-camp/resources/assets/images/insegna.jpg"/>
    </div>
    <div class="flex items-center">

        <div class="w-full lg:w-1/2 lg:mx-auto">
            <div class="flex items-center">
                <div class="w-full md:mx-auto">
                    @foreach ($catalogs as $catalog)
                        <livewire:show-catalog :cid="$catalog->id"/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
