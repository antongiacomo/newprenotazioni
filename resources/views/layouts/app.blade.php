<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    @livewireStyles
</head>
<body class="bg-cream-200 h-screen antialiased leading-none bg-pattern fill-current">
<div id="app">
    @auth
    <nav class="bg-blue-900 shadow mb-8 py-6">
        <div class="container mx-auto px-6 md:px-0">
            <div class="flex flex-wrap items-center justify-center">
                <div class="mr-4 p-4 rounded hover:bg-blue-100 text-gray-100  hover:text-blue-900">
                    <a href="{{ url('/') }}"
                       class="text-lg font-semibold no-underline">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>
                <div class="mr-6 p-4 rounded hover:bg-blue-100 text-gray-100  hover:text-blue-900">
                    <a href="{{ route('products.create') }}"
                       class="text-lg font-semibold no-underline">
                        Prodotti
                    </a>
                </div>
                <div class="mr-6 p-4 rounded hover:bg-blue-100 text-gray-100  hover:text-blue-900">
                    <a href="{{ route('companies.index') }}"
                       class="text-lg font-semibold no-underline ">
                        Aziende
                    </a>
                </div>
                <div class="mr-6 p-4 rounded hover:bg-blue-100 text-gray-100  hover:text-blue-900">
                    <a href="{{ route('catalog.index') }}"
                       class="text-lg font-semibold no-underline ">
                        Listino
                    </a>
                </div>
                <div class="mr-6 p-4 rounded hover:bg-blue-100 text-gray-100  hover:text-blue-900">
                    <a href="{{ route('database.index') }}" class="text-lg font-semibold  no-underline">
                        Ricerca Prodotti
                    </a>
                </div>
                <div class="flex-1 text-right">
                    @guest
                        <a class="no-underline hover:underline text-gray-300 text-sm p-3"
                           href="{{ route('login') }}">{{ __('Login') }}</a>
                        @if (Route::has('register'))
                            <a class="no-underline hover:underline text-gray-300 text-sm p-3"
                               href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    @else
                        <span class="text-gray-300 text-sm pr-4">{{ Auth::user()->name }}</span>

                        <a href="{{ route('logout') }}"
                           class="no-underline hover:underline text-gray-300 text-sm p-3"
                           onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                            {{ csrf_field() }}
                        </form>
                    @endguest
                </div>
            </div>
        </div>
    </nav>
    @endauth
    @yield('content')
</div>
@livewireScripts
@yield('script')
</body>
</html>
