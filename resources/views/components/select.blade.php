<label class="block @if(!($inline ?? false)) mb-6 @else w-full @endif">

    @isset($label)
        <span class="block text-base @if(!($inline ?? false)) mb-3 @endif">
      {{ $label }}
    </span>
    @endisset

    <select
            {{ $attributes->merge(['class' => 'form-select text-base d:bg-gray-500 py-0 h-9 d:text-gray-100 placeholder-gray-400 border-none']) }}
            name="{{$name}}"
    >
        {{ $slot }}
    </select>



</label>
