@extends('layouts.app') @section('content')
    <div class="grid grid-cols-1 md:grid-cols-2 gap-2 px-4">
        <div class="bg-white rounded-lg shadow">
            <livewire:create-catalog/>
        </div>
        <div class="bg-white rounded-lg shadow">
            <livewire:show-catalog/>
        </div>
    </div>
    <div class="flex flex-wrap p-4">

    </div>

@endsection
