<div>
    @if($catalog)
        @if($catalog->variations->count() > 0)
            <div class="bg-white rounded-lg shadow-lg m-4 p-6">
                <div class="border-b p-4">
                    <p class="text-3xl uppercase font-semibold text-red-700 mb-3">
                        {{$catalog->company->name}}
                    </p>
                    <div class="text-black font-medium">
                        <div class="text-xl my-2">ordinare entro
                            il {{$catalog->expires_at->format('d/m')}} <span
                                    class="text-lg">({{$catalog->expires_at->diffForHumans(\Carbon\Carbon::now(),['syntax' => \Carbon\CarbonInterface::DIFF_ABSOLUTE]) }})</span>
                        </div>

                        <div class="text-xl my-2">ritiro in negozio
                            il {{$catalog->expires_at->format('d/m')}} <span
                                    class="text-lg">({{$catalog->pick_at->diffForHumans(\Carbon\Carbon::now(),['syntax' => \Carbon\CarbonInterface::DIFF_ABSOLUTE]) }})</span>
                        </div>
                    </div>
                </div>

                <div class="p-4">

                    @foreach ($catalog->variations->groupBy('product.name') as $key => $products)


                        <div class="flex items-center border-b ">
                            <div class="w-24 py-2">
                                <img
                                        class="rounded-lg object-cover"
                                        src="{{$products->first()->product->image}}"
                                />

                            </div>

                            <div class="p-4 w-full">
                                <h1 class="text-2xl font-medium">

                                    {{$key}}
                                </h1>
                                <h1 class="text-xl text-gray-500 my-2">
                                    {{$products->first()->product->description}}
                                </h1>
                            </div>
                        </div>
                        @foreach ($products as $variation)

                            <div class="my-4">
                                <div class="my-4  flex border-b border-gray-300">
                                    <h1 class="w-full text-xl font-bold">
                                        {{$variation->description}}
                                    </h1>
                                    <p class="w-full text-lg">
                                        @money($variation->price)
                                    </p>
                                    <p class="w-full text-lg">
                                        circa {{$variation->pivot->quantity}}
                                        {{$variation->pivot->unit}}
                                    </p>
                                    @auth
                                        <div>

                                            <div wire:key="{{$variation->id}}{{$loop->index}}"
                                                 wire:click="delete({{ $variation->id }}')" class="cursor-pointer">
                                                <svg viewBox="0 0 20 20" fill="currentColor"
                                                     class="text-red-500 x-circle w-6 h-6 mr-4">
                                                    <path fill-rule="evenodd"
                                                          d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                                                          clip-rule="evenodd"></path>
                                                </svg>
                                            </div>

                                        </div>
                                    @endauth
                                </div>

                            </div>

                        @endforeach


                    @endforeach

                </div>
            </div>
        @endif
    @endif
</div>
