<div class="border-b pb-4 mb-4">
    {{$startAt}}
    {{$cid ?? 'NO id'}}
    <div class="my-8 flex flex-wrap">
        @foreach($catalogs as $aCatalog)
            <div class="bg-blue-300  m-1 p-2 rounded cursor-pointer" wire:click="setCid({{ $aCatalog->id }})">
                {{$aCatalog->id}}
            </div>

        @endforeach
        <button wire:click="create"
                class="bg-blue-900 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
            Aggiungi

        </button>
    </div>
    @if($cid)
        <div class="flex flex-1 items-center space-x-2 my-2 ">

            <div class="flex flex-wrap mb-2">

                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-city">
                        Inizia il
                    </label>
                    <input wire:model="startAt" class="appearance-none picker"
                           id="grid-city" type="text">
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                        Scade il
                    </label>
                    <input wire:model="expireAt" class="appearance-none picker" type="text">
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-zip">
                        Nascondi il
                    </label>
                    <input wire:model="hideAt" class="appearance-none picker" type="text">
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-4">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-zip">
                        Ritiro il
                    </label>
                    <input wire:model="pickAt" class="appearance-none picker" type="text">
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Azienda
                        {{$company}}
                    </label>
                    <select
                            wire:model="company"
                            class="focus:outline-none focus:shadow-outline"
                            id="product" type="text" placeholder="Prodotto">
                        @foreach ($companies as $company)
                            <option value="{{$company->id}}" {{ $catalog->company->id == $company->id ? "selected":"" }}>{{$company->name}}</option>

                        @endforeach


                    </select>
                </div>
                <div class="w-full md:w-1/2 px-3 mb-6 md:mb-4">

                    <button wire:click="delete"
                            class="bg-red-700 hover:bg-red-900 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                        Cancella

                    </button>
                </div>


                @foreach( $products as $product )
                    <livewire:add-variation :key="'var_' . $product->id" :cid="$cid" :vid="$product->id"/>
                @endforeach

            </div>


        </div>
    @endif
</div>

@section('script')

    <script>
        document.addEventListener('DOMContentLoaded', (event) => {
            flatpickr(".picker", {
                enableTime: true,
                dateFormat: "d-m-Y H:i",

            });

        });

    </script>
@endsection
