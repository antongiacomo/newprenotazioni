<div class="w-full px-4">


    <div class="flex items-center space-x-2 space-y-2 my-2 s">

        <div class="w-1/3 text-xl">
            {{$variation->description}}

        </div>
        <div class="flex w-1/3">
            <div class=" text-xl">
                <input wire:model="quantity" type="text"
                       class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                @error('quantity') <span class="error">{{ $message }}</span> @enderror
            </div>


            <div class="flex text-xl">
                @foreach (\App\Catalog::UNITS as $unit)
                    <label wire:key="{{$variation->id}}{{$loop->index}}" for="{{$unit}}"
                           class="{{$unit == $this->unit ? 'bg-blue-800' : 'bg-blue-400'}} {{ $loop->first ? 'rounded-l' : '' }}  {{ $loop->last ? 'rounded-r' : '' }}  hover:bg-blue-700 text-blue-100  flex-1 p-2 py-2">
                        <input type="radio" wire:model="unit" value="{{$unit}}"
                               id="{{$unit}}" style="display:none"/>

                        <span class="font-semibold flex-auto">{{$unit}}</span>
                    </label>
                @endforeach
                @error('unit') <span class="error">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="w-1/3">
            <button wire:click="save"
                    class="w-full bg-blue-900 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                Aggiungi al listino {{$cid}}

            </button>

        </div>

    </div>


</div>

