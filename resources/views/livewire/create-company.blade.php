<div>
    <div class="bg-white border shadow rounded-lg items-center my-3 p-4">

        <form wire:submit.prevent="addCompany">
            <div class="w-full flex">

                <div class="flex-1 text-xl">
                    <input wire:model="name"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           type="text">
                </div>

                <div class="mx-2">
                    <button type="submit"
                            class="w-full bg-blue-900 text-white font-bold py-2 px-4 rounded  leading-tight focus:outline-none focus:shadow-outline">
                        Inserisci
                    </button>
                </div>
            </div>
        </form>

        @error('name')
        <div class="text-red-500 mt-2">{{ $message }}</div> @enderror
    </div>
    @foreach ($companies as $company)
        <div class="w-full flex bg-white border shadow rounded-lg items-center my-3 p-4" wire:key="{{$company->id}}">

        <!--<div class="flex-1">
                {{ $company->id }}
                </div>-->
            <div class="flex-1 text-xl">
                {{ $company->name }}
            </div>
            <livewire:delete-button :obj="$company" :key="'d_' . $company->id"/>

        </div>
    @endforeach
</div>
