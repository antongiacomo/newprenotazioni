<div class="relative">
    <input
            type="text"
            class="form-input"
            placeholder="Carca Prodotto..."
            wire:model="query"
            wire:keydown.escape="clear"
            wire:keydown.tab="clear"
            wire:keydown.arrow-up="decrementHighlight"
            wire:keydown.arrow-down="incrementHighlight"
            wire:keydown.enter="selectContact"
    />

    <div wire:loading class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg">
        <div class="list-item">Searching...</div>
    </div>

    @if(!empty($query))
        <div class="fixed top-0 right-0 bottom-0 left-0" wire:click="clear"></div>

        <div class="absolute z-10 list-group bg-white w-full rounded-t-none shadow-lg">
            @if(!empty($products))
                @foreach($products as $i => $product)

                    <p wire:click="selectContact" class="p-4 flex hover:bg-blue-300 cursor-pointer items-center h-24">
                        <img class="object-center object-contain w-1/6 h-24" src="{{$product->thumb}}">
                        <a class="text-xl w-full text-center list-item {{ $highlightIndex === $i ? 'bg-blue-300' : '' }}">
                            {{ $product['nome_prodotto'] }}

                        </a>
                    </p>
                @endforeach
            @else
                <div class="list-item">No results!</div>
            @endif
        </div>
    @endif
</div>