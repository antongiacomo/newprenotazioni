<div class="border-t p-4">
    <span class="font-medium text-gray-500">Aggiungi Variazione</span>
    <form wire:submit.prevent="submit" class="mt-4 flex space-x-2">
        <div class="w-full">
            <input type="text" wire:model="description" placeholder="Descrizione">
            @error('description') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="w-full">
            <input type="text" wire:model="price" placeholder="Prezzo">
            @error('price') <span class="error">{{ $message }}</span> @enderror
        </div>
        <button class="hover:bg-blue-700 focus:outline-none focus:shadow-outline" type="submit">Aggiungi</button>
    </form>
</div>
