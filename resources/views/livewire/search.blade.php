<div class="mb-8">
    <input type="text"
           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
           placeholder="Filtra..."
           wire:model.debounce.500ms="searchTerm"/>
    <p>{{$image->count()}}</p>
    @if ($searchTerm and $image)
        @foreach($image as $product)
            <div class="flex m-2 border-t-2 bg-white rounded-lg">
                <img class="object-center object-contain w-1/6" src="{{$product->thumb}}">
                <table>
                    @foreach(array_keys($product->toArray()) as $key)

                        <tr class="border">
                            <td class="font-bold"> {{$key}} </td> <td>{{$product->$key}}</td>
                        </tr>
                    @endforeach

                </table>
            </div>
        @endforeach
    @endif
</div>
