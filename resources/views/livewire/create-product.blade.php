<div class="container mx-auto">

    <div class="grid grid-cols-1  md:grid-cols-2 gap-4 ">

        <div>
            <livewire:search-products/>
            <form wire:submit.prevent="save" class="bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-6 ">
                <div class="mb-6">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Prodotto
                    </label>
                    <input wire:model="name"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="product" type="text" placeholder="Prodotto">
                    @error('name') <span class="error">{{ $message }}</span> @enderror
                </div>
                <div class="mb-6">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Azienda
                    </label>
                    <select
                            wire:model="company"
                            class="focus:outline-none focus:shadow-outline"
                            id="product" type="text" placeholder="Prodotto">
                        @foreach ($companies as $company)
                            <option value="{{$company->id}}" {{ $lastUsedCompany->id == $company->id ? "selected":"" }}>{{$company->name}}</option>
                        @endforeach

                    </select>
                    @error('company') <span class="error">{{ $message }}</span> @enderror
                </div>
                <div class="mb-6">

                    <textarea wire:model="description"
                              class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                              placeholder="Breve Descrizione"></textarea>
                    @error('descriptions') <span class="error">{{ $message }}</span> @enderror

                </div>
                <div class="mb-6">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Prezzo
                    </label>
                    <input wire:model="price"
                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="product" type="text" placeholder="Prezzo">
                    @error('price') <span class="error">{{ $message }}</span> @enderror

                </div>

                <div class="mb-6">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="image">
                        Immagine
                    </label>
                    <!-- component -->

                    <div class="flex w-full items-center justify-center bg-grey-lighter">
                        <label class="w-64 flex flex-col items-center px-4 py-6 bg-white text-blue-900 rounded-lg shadow-lg tracking-wide uppercase border border-blue-900 cursor-pointer hover:bg-blue-900 hover:text-white">
                            @if ($image and !$errors->has('image'))
                                <span>Anteprima:</span>
                                <img class="m-4" src="{{ $image->temporaryUrl() }}">
                            @else
                                <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 20 20">
                                    <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z"/>
                                </svg>
                            @endif


                            <span class="mt-2 text-base leading-normal">Select a file</span>
                            <input type="file" id="image" class="hidden" wire:model="image">
                            @error('image') <span class="error">{{ $message }}</span> @enderror
                        </label>
                    </div>


                </div>
                <div class="flex items-center justify-between">
                    <button class="w-full bg-blue-900 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg focus:outline-none focus:shadow-outline"
                            type="submit">
                        Aggiungi
                    </button>

                </div>
            </form>

        </div>
        <div class="h-screen overflow-y-scroll" style="height: calc(100vh - 80px);">
            <div class="sticky top-0">
                <input class="shadow appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                       placeholder="Filtra...
"
                       wire:model="search">
            </div>
           @include('products.list')

        </div>

    </div>
</div>
