<div class="flex flex-col break-words bg-transparent">

    @foreach ($products as $key => $product)
        <div class="w-full mt-2 bg-white border shadow rounded-lg">
            <div class="flex items-center ">
                <div class="flex-1">
                    <img class="w-32 h-24 rounded-tl object-cover" src="{{ $product->image }}"/>
                </div>

                <div class="flex-1">
                    {{ $product->name }}
                </div>
                <div class="flex-1">
                    @money($product->variations->first()->price ?? 0)
                </div>
                <div class="flex-1 flex items-center">
                    <div class="w-3 h-3 rounded-full mr-1"
                         style="background-color: {{$product->company->color}}">

                    </div>
                    {{ $product->company->name }}
                </div>

                <livewire:delete-button :obj="$product" :key="$product->id"/>
            </div>
            <div class="border-t  pl-4 py-2">
                @foreach ($product->variations as $availability)
                    <div class="flex items-center {{ $loop->last ? '' : 'border-b' }} border-dotted">
                        <p class="w-full"> {{$availability->description}}</p>
                        <p class="w-full">@money($availability->price)/{{$availability->unit}}</p>
                        <livewire:delete-button :obj="$availability" key="a_{{$availability->id}}"/>
                    </div>

                @endforeach
            </div>

            <livewire:create-variation :pid="$product->id" :key="'p_'.$product->id"/>
        </div>

    @endforeach

</div>