@extends('layouts.app') @section('content')
    <div class="flex items-center">
        <div class="w-full md:w-1/2 md:mx-auto">

           @include('products.list')
        </div>
    </div>

@endsection
