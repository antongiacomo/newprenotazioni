<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'catalogs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->dateTime('show_at')->default(now());
            $table->dateTime('expires_at')->nullable()->default(null);
            $table->dateTime('hide_at')->nullable()->default(null);
            $table->dateTime('pick_at')->nullable()->default(null);

        }
        );
        Schema::create(
            'catalog_variation', function (Blueprint $table) {
            $table->bigInteger('catalog_id')->unsigned();
            $table->bigInteger('variation_id')->unsigned();
            $table->foreign('catalog_id')
                ->references('id')
                ->on('catalogs')
                ->onDelete('cascade');
            $table->foreign('variation_id')
                ->references('id')
                ->on('variations')
                ->onDelete('cascade');


            $table->integer('quantity')->default(0)->nullable();
            $table->string('unit')->default('kg')->nullable();

        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog_variation');
        Schema::dropIfExists('catalogs');


    }

}
