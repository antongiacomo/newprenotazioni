<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'availabilities', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('variation_id')->constrained()->onDelete('cascade');
            $table->integer('quantity')->default(0)->nullable();
            $table->string('unit')->default('kg')->nullable();
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availabilities');
    }
}
