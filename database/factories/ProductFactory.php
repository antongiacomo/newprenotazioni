<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(
    App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'image' => $faker->imageUrl(320,320),
        'company_id' => 1,
        'price' => $faker->numberBetween($min = 200, $max = 9000)

    ];
}
);