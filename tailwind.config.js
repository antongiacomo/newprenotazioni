module.exports = {
    purge: ["./resources/views/**/*.blade.php", "./resources/css/**/*.css"],
    theme: {
        extend: {
            fontFamily: {
                sans: "Karla, sans-serif"
            },
            colors: {
                'cream': {
                    50: '#FEFDFA',
                    100: '#FCFBF5',
                    200: '#F8F6E5',
                    300: '#F3F0D5',
                    400: '#EBE4B6',
                    500: '#E2D996',
                    600: '#CBC387',
                    700: '#88825A',
                    800: '#666244',
                    900: '#44412D',
                },
            }
        }
    },
    variants: {},
    plugins: [require("@tailwindcss/ui")]
};
