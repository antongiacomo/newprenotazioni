<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Catalog
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $show_at
 * @property \Illuminate\Support\Carbon|null $expires_at
 * @property \Illuminate\Support\Carbon|null $hide_at
 * @property \Illuminate\Support\Carbon|null $pick_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Variation[] $variations
 * @property-read int|null $variations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog query()
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog whereHideAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog wherePickAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog whereShowAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Catalog whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Catalog extends Model
{
    const UNITS = ['kg','g','pz'];
    protected $dates = ['expires_at', 'hide_at','pick_at'];


    public function variations(){
        return $this->belongsToMany(Variation::class)->withPivot(['quantity','unit']);
    }
    public function getCompanyAttribute(){
        return $this->variations->first()->product->company ?? Company::lastUsed();
    }
}
