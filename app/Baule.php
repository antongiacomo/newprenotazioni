<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Baule
 *
 * @property int|null    $codice_num
 * @property string|null $nome_prodotto
 * @property string|null $marchio
 * @property string|null $pz_kg
 * @property int|null    $cartone
 * @property int|null    $ordine_minimo
 * @property string|null $confezione
 * @property string|null $peso_sgocciolato
 * @property string|null $ui
 * @property string|null $contenuto_unità_a_peso_variabile
 * @property string|null $macro_categorie
 * @property string|null $categorie
 * @property string|null $sotto_categorie
 * @property float|null  $prezzo_base
 * @property string|null $prezzo_base_spezzato
 * @property float|null  $prezzo_ridotto
 * @property string|null $prezzo_ridotto_spezzato
 * @property float|null  $prezzo_consigliato
 * @property float|null  $marginalità
 * @property int|null    $iva
 * @property string|null $codice_a_barre
 * @property string|null $prov
 * @property string|null $ingredienti
 * @property-read mixed  $image
 * @property-read mixed  $thumb
 * @method static Builder|Baule newModelQuery()
 * @method static Builder|Baule newQuery()
 * @method static Builder|Baule query()
 * @method static Builder|Baule whereCartone($value)
 * @method static Builder|Baule whereCategorie($value)
 * @method static Builder|Baule whereCodiceABarre($value)
 * @method static Builder|Baule whereCodiceNum($value)
 * @method static Builder|Baule whereConfezione($value)
 * @method static Builder|Baule whereContenutoUnitàAPesoVariabile($value)
 * @method static Builder|Baule whereIngredienti($value)
 * @method static Builder|Baule whereIva($value)
 * @method static Builder|Baule whereMacroCategorie($value)
 * @method static Builder|Baule whereMarchio($value)
 * @method static Builder|Baule whereMarginalità($value)
 * @method static Builder|Baule whereNomeProdotto($value)
 * @method static Builder|Baule whereOrdineMinimo($value)
 * @method static Builder|Baule wherePesoSgocciolato($value)
 * @method static Builder|Baule wherePrezzoBase($value)
 * @method static Builder|Baule wherePrezzoBaseSpezzato($value)
 * @method static Builder|Baule wherePrezzoConsigliato($value)
 * @method static Builder|Baule wherePrezzoRidotto($value)
 * @method static Builder|Baule wherePrezzoRidottoSpezzato($value)
 * @method static Builder|Baule whereProv($value)
 * @method static Builder|Baule wherePzKg($value)
 * @method static Builder|Baule whereSottoCategorie($value)
 * @method static Builder|Baule whereUi($value)
 * @mixin \Eloquent
 */
class Baule extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'test';
    protected $primaryKey = 'codice_num';

    public function getthumbAttribute()
    {
        return sprintf(
            "https://areabusiness.bvfdl.it/archives/article/thumb_%s_1.jpg",
            $this->codice_num
        );
    }

    public function getImageAttribute()
    {
        return sprintf(
            "https://areabusiness.bvfdl.it/archives/article/%s_1.jpg",
            $this->codice_num
        );
    }

}
