<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Shahonseven\ColorHash;

/**
 * App\Company
 *
 * @property int                             $id
 * @property string                          $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed                      $color
 * @method static \Illuminate\Database\Eloquent\Builder|Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Company extends Model
{
    public function getColorAttribute()
    {

        $colorhash = new ColorHash();
        return $colorhash->hex($this->name);

    }

    public static function lastUsed()
    {
        return Company::find(Product::select('company_id')->orderBy('created_at', 'desc')
                ->limit(1)->get()->first()->company_id ?? 0);
    }

}
