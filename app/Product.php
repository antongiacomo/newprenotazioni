<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Product
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string $image
 * @property int $company_id
 * @property int $price
 * @property-read \App\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Variation[] $variations
 * @property-read int|null $variations_count
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCompanyId($value)
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereImage($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product wherePrice($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $description
 * @method static Builder|Product whereDescription($value)
 */
class Product extends Model
{
    public function getImageAttribute($image)
    {
        return str_replace("public", "/storage", $image);

    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function variations()
    {
        return $this->hasMany('App\Variation');
    }


    protected static function boot()
    {
        parent::boot();
        // Order by name ASC
        static::addGlobalScope(
            'order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        }
        );
    }
}
