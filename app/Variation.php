<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Variation
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $product_id
 * @property int|null $price
 * @property string|null $description
 * @property-read \App\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|Variation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Variation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Variation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Variation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variation wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variation whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Variation whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Availability[] $availabilities
 * @property-read int|null $availabilities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Catalog[] $catalogs
 * @property-read int|null $catalogs_count
 */
class Variation extends Model
{
    //
    protected $fillable = array('description', 'price', 'product_id');

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function setPriceAttribute($value) {
        $this->attributes['price'] = $value * 100;
    }
    public function catalogs(){
        return $this->belongsToMany(Catalog::class)->withPivot(['quantity','unit']);;
    }


}
