<?php

namespace App\Http\Livewire;

use App\Catalog;
use Livewire\Component;

class ShowCatalog extends Component
{
    protected $listeners = ['catalogCreated' => 'catalogCreated'];
    //protected $queryString = ['cid'];
    public $cid;

    public function render()
    {
        return view(
            'livewire.show-catalog', ['catalog' => Catalog::find($this->cid)]
        );
    }


    public function delete($variation)
    {
        Catalog::find($this->cid)->variations()->detach();

    }


    public function catalogCreated($cid)
    {
        $this->cid = $cid;
        $refresh;
    }
}
