<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DeleteButton extends Component
{
    public $obj;
    public function mount($obj)
    {
        $this->obj = $obj;
    }
    public function render()
    {
        return view('livewire.delete-button');
    }

    public function delete(){

        $this->obj->delete();
        $this->emitUp('somethingDeleted');
    }
}
