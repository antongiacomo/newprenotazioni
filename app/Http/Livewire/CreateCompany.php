<?php

namespace App\Http\Livewire;

use App\Company;
use Livewire\Component;

class CreateCompany extends Component
{
    public $name;
    protected $listeners = ['somethingDeleted' => '$refresh'];

    public function addCompany(){
        $this->validate(
            [
                'name' => 'required|unique:companies'
            ]
        );
        $company = new Company();
        $company->name = $this->name;
        $company->save();
        $this->name = "";
    }
    public function render()
    {
        $companies = Company::all();
        return view('livewire.create-company', compact('companies'));
    }
}
