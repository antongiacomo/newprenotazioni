<?php

namespace App\Http\Livewire;

use App\Baule;
use App\Company;
use App\Product;
use App\Variation;
use Livewire\Component;
use Livewire\WithFileUploads;

class CreateProduct extends Component
{
    use WithFileUploads;

    public $company;
    public $image;
    public $name;
    public $search;
    public $price;
    public $description;
    protected $listeners = ['somethingDeleted' => '$refresh','variantAdded' => '$refresh', 'selected' => 'productSelected'];

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }
    public function  mount(){
      $this->company  = Company::all()->first()->id ?? '' ;
    }

    public function productSelected(Baule $product){
        $this->name = $product->nome_prodotto;
        $this->price =  $product->prezzo_consigliato;
        $this->description = $product->ingredienti;



    }

    public function save()
    {


        $this->validate(
            [
                'name' => 'required',
                'image' => 'nullable|image|max:8192',
                'company' => 'required'

            ]
        );

        $product = new Product;
        $product->company_id = $this->company;
        $product->name = $this->name;
        $product->description = $this->description;
        if ($this->image != null) {
            $product->image = $this->image->store('public');
        } else {
            $product->image
                = "https://www.cowgirlcontractcleaning.com/wp-content/uploads/sites/360/2018/05/placeholder-img.jpg";
        }
        $product->save();

        $variation = new Variation();
        $variation->description = $this->name;
        $variation->price = $this->price;
        $variation->product()->associate($product);
        $variation->save();


    }



    public function render()
    {

        $products = Product::all();

        if(!empty( $this->search)){
            $products = Product::where('name', 'like', '%'.$this->search.'%')->get();
        }

        return view(
            'livewire.create-product', [
                'products' => $products,
                'companies' => Company::all(),
                'lastUsedCompany' =>Company::lastUsed()
            ]
        );

    }
}
