<?php

namespace App\Http\Livewire;

use App\Baule;

use Livewire\Component;

class SearchProducts extends Component
{
    public $query = '';
    public $products = [];
    public $highlightIndex = 0;

    public function mount()
    {
        $this->reset(['query', 'products', 'highlightIndex']);
    }

    public function clear()
    {
        $this->reset(['query', 'products', 'highlightIndex']);
    }

    public function incrementHighlight()
    {
        if ($this->highlightIndex === count($this->products) - 1) {
            $this->highlightIndex = 0;
            return;
        }
        $this->highlightIndex++;
    }

    public function decrementHighlight()
    {
        if ($this->highlightIndex === 0) {
            $this->highlightIndex = count($this->products) - 1;
            return;
        }
        $this->highlightIndex--;
    }

    public function selectContact()
    {
        $product = $this->products[$this->highlightIndex] ?? null;
        if ($product) {
            $this->emitUp('selected', $product);
        }
        $this->clear();

    }

    public function updatedQuery()
    {
        $this->products = Baule::where(
            'nome_prodotto', 'like', '%' . $this->query . '%'
        )
            ->get();
    }

    public function render()
    {
        return view('livewire.search-products');
    }
}