<?php

namespace App\Http\Livewire;

use App\Baule;
use Livewire\Component;

class Search extends Component
{
    protected $queryString = ['searchTerm'];
    public $searchTerm = "gatti manzo e cicoria";
    public $image = [];

    public function render()
    {


        if (strlen($this->searchTerm) >= 3) {
            $this->image = sprintf(
                "https://areabusiness.bvfdl.it/archives/article/thumb_%s_1.jpg",
                $this->searchTerm
            );
            $this->image = Baule::where(
                'nome_prodotto', 'like', '%' . $this->searchTerm . '%'
            )
                ->orWhere('codice_num', 'like', $this->searchTerm . '%')
                ->orWhere('codice_a_barre', 'like', $this->searchTerm . '%')
                ->orWhere('marchio', 'like', '%' . $this->searchTerm . '%')

                ->get();
        }
        return view('livewire.search');
    }
}
