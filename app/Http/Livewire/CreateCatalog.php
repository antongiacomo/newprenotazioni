<?php

namespace App\Http\Livewire;

use App\Availability;
use App\Catalog;
use App\Company;
use App\Variation;
use Carbon\Carbon;
use Livewire\Component;

class CreateCatalog extends Component
{
    protected $queryString = ['cid'];
    public $cid;
    public $catalog;
    public $unit = "kg";
    public $quantity = 0;
    public $company;
    public $startAt = "";
    public $expireAt = "";
    public $hideAt = "";
    public $pickAt = "";


    public function mount()
    {

        $this->startAt = Carbon::now()->format('d-m-Y H:i');
        $this->expireAt = Carbon::parse($this->startAt)
            ->addDays(3)
            ->format('d-m-Y H:i');
        $this->pickAt = Carbon::parse($this->expireAt)
            ->next("9:30")
            ->format('d-m-Y H:i');
        $this->hideAt = Carbon::parse($this->expireAt)
            ->next("22:30")
            ->format('d-m-Y H:i');

    }


    public function create()
    {

        $av = new Catalog();


        /*  $av->variations()->attach(
              Variation::findOrFail($this->variation_id),
              ['quantity' => $this->quantity, 'unit' => $this->unit]
          );*/

        $this->cid = $av->id;
        $this->catalog = $av;

        $this->catalog->show_at = Carbon::parse($this->startAt);
        $this->catalog->expires_at = Carbon::parse($this->expireAt);
        $this->catalog->hide_at = Carbon::parse($this->hideAt);
        $this->catalog->pick_at = Carbon::parse($this->pickAt);

        $av->save();
        $this->emit('catalogCreated', $this->cid);


    }


    public function render()
    {
        $this->catalog = Catalog::find($this->cid);
        return view(
            'livewire.create-catalog', [

                'products' => Variation::whereHas(
                    'product', function ($query) {
                    return $query->where('company_id', '=', $this->company);
                }
                )->get()
                ,
                'catalogs' => Catalog::all(),
                'companies' => Company::all(),
                'lastUsedCompany' => Company::lastUsed()
            ]
        );

    }

    public
    function setCid(
        $id
    )
    {
        $this->cid = $id;
        $this->emit('catalogCreated', $this->cid);
    }

    public
    function delete()
    {
        Catalog::findOrFail($this->cid)->delete();
        $this->cid = "";
    }
    /* public function dehydrateStartAt()
     {
         $this->startAt = $this->startAt->format('Y-m-d H:i');
     }

     public function hydrateStartAt()
     {

         $this->startAt = Carbon::parse($this->startAt);
     }
     /*
     public function dehydrateExpireAt()
     {
         $this->expireAt = $this->expireAt->format('Y-m-d H:i');
     }
     public function dehydratePickAt()
     {
         $this->pickAt = $this->pickAt->format('Y-m-d H:i');
     }
      */
}
