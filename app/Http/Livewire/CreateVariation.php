<?php

namespace App\Http\Livewire;

use App\Product;
use App\Variation;
use Livewire\Component;

class CreateVariation extends Component
{
    public $description;
    public $price;
    public $pid;
    public function mount($pid)
    {
        $this->pid = $pid;
    }
    public function submit()
    {

        $this->validate(
            [
                'description' => 'required|min:1',
                'price' => 'required|numeric',
            ]
        );
        // Execution doesn't reach here if validation fails.

        Variation::create(
            [
                'description' => $this->description,
                'price' => $this->price,
                'product_id' => $this->pid
            ]
        );
        $this->emit('variantAdded');
        $this->description = "";
        $this->price = "";
    }


    public function render()
    {
        return view('livewire.create-variation');
    }
}
