<?php

namespace App\Http\Livewire;

use App\Catalog;
use App\CatalogTrait;
use App\Variation;
use Livewire\Component;

class AddVariation extends Component
{
    
    public $variation;
    public $unit = 'kg';
    public $quantity;
    public $cid;

    protected $listeners = ['catalogCreated' => 'catalogCreated'];

    public function mount($cid,$vid)
    {
        $this->cid = $cid;
        $this->variation = Variation::findOrFail($vid);
    }

    public function render()
    {

        return view('livewire.add-variation', ['variation' => $this->variation]);
    }


    public function catalogCreated($cid)
    {
        $this->cid = $cid;
    }




    public function save()
    {

        $av = Catalog::findOrNew($this->cid);
        $av->variations()->attach(
            $this->variation,
            ['quantity' => $this->quantity, 'unit' => $this->unit]
        );
        $this->emit( 'catalogCreated', $this->cid);
    }
}
