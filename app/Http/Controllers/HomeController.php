<?php

namespace App\Http\Controllers;

use App\Catalog;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class HomeController extends Controller
{
    public function ExportPDF()
    {
        $catalogs = Catalog::all();
        $pdf = PDF::loadView('welcome', compact('catalogs'));
        return $pdf->stream('catalog.pdf');

    }

    public function index()
    {
        $catalogs = Catalog::all();
        return view('welcome', compact('catalogs'));
    }
}
